from receipts.views import (
    receipts_list,
    create_receipt,
    category_list,
    account_list,
)
from django.urls import path

urlpatterns = [
    path("", receipts_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
]
